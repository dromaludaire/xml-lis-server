% Documentation de la couche basse de Portalis
% Benjamin Sigonneau
% mai 2014


Présentation de Portalis
========================

Portalis est une application web collaborative qui permet à des
utilisateurs distants de se partager des serveurs d’information logique
LIS basés sur Camelis. Portalis est construit à partir de modules
réutilisables, de sorte qu’il est possible à des utilisateurs de
construire leurs propres clients.

Voici le schéma de cette application :

![Architecture globale de Portalis](portalis-schema.png)

On distingue trois types de composants :

- les serveurs LIS, à droite : ils offrent une abstraction de Camelis
  accessible à travers une interface HTTP/XML ;
- le portail, au milieu : il offre un ensemble de composants Java permettant de
  construire des applications utilisant les LIS, en offrant une
  abstraction du protocole d'interrogation des serveurs LIS. Le portail
  utilise ces composants pour fournir un point d'accès central, qui
  permet d'utiliser plusieurs serveurs LIS en même temps ;
- les clients, à gauche : ce sont les applications manipulées par
  l'utilisateur final. Ils peuvent être génériques ou bien spécifiques.
  Ils peuvent être écrits soit en utilisant les composants Java fournies
  par le portail (parties bleues du schéma), soit en utilisant
  directement le protocole HTTP/XML des serveurs LIS (partie verte du
  schéma).


Dans la suite de ce document, nous nous intéressons à la couche basse de
Portalis, c'est-à-dire aux serveurs LIS, que nous nommons
`xml-lis-server`. Concrètement, il s'agit d'un serveur HTTP autonome qui
permet d'utiliser l'API de Camelis en posant des requête HTTP. Les
réponses sont au format XML.

Cette documentation décrit l'installation de `xml-lis-server`, son
lancement, l'API HTTP qui permet de l'interroger et le format des
réponses.


Installation de `xml-lis-server`
================================

Prérequis :

    - bibliothèque Camelis
    - Ocamlnet (avec la bibliothèque nethttpd)
    - imagemagick (pour la création des miniatures d'images)

Récupération des sources et compilation : les sources de
`xml-lis-server` sont contenues dans les sources de Portalis.
Celles-ci sont disponibles sur un dépôt BitBucket :

    git clone https://bitbucket.org/bekkers/portalis.git
    cd portalis/camelis-server/src/main/ocaml
    make

La compilation produit un exécutable que l'on peut directement utiliser.
Il peut être intéressant de le recopier dans un répertoire figurant dans
`$PATH`, par exemple :

    sudo cp xml-lis-server.exe /usr/local/bin/xml-lis-server



Lancement de `xml-lis-server`
=============================

Au lancement, `xml-lis-server` requiert trois paramètres obligatoires :

  - port : le numéro de port sur lequel le server écoutera
  - key : clé d'identification de l'administrateur du contexte (chaîne
    hexadécimale). Cette clé n'expire jamais.
  - datadir : chemin où trouver le contexte

Un paramètre est optionnel :

  - log : nom absolu du fichier de log (par défaut : `/tmp/portalis.log`)

Un autre paramètre optionnel est présent à des fins de test, et ne doit
donc pas être utilisé dans un cadre d'utilisation classique :

  - despot : ignorer les droits utilisateurs

Enfin, le paramètre `-help` permet d'afficher le synopsis de
`xml-lis-server` et la liste de ses paramètres.


Exemple :

xml-lis-server -port 9999 -key 1234abcd -log /srv/logs/camelisServers.log -datadir /srv/webapps/formalConcepts/colors

Cette commande crée un serveur Lis qui écoute sur le port 9999. Le
contexte se trouve dans `/srv/webapps/formalConcepts/colors`. Le
contexte sera recherché dans le fichier `colors.lis` (ou `colors.ctx`, à
défaut) et automatiquement chargé. À défaut un contexte vide de ce nom
sera créé.


Session de test : le makefile de xml-lis-server contient une cible de
test qui installe un contexte de test (basé sur les planètes du système
solaire) et lance `xml-lis-server` sen écoute sur le port 9999. Pour
lancer cette instance de test, il suffit d'exécuter :

    make runtest

Dans la suite de ce texte, les exemples d'appels aux commandes de
`xml-lis-server` sont en général pris sur cette instance de test. Les
liens fournis sont donc cliquables si l'instance de test a été lancée.


Index des commandes
===================


+-------------------------------------------------------------+---------------------------------------------+
| Section                                                     | Commandes                                   |
+=============================================================+=============================================+
| [Commandes générales](#commandes-générales)                 | [ping](#ping)                               |
+-------------------------------------------------------------+---------------------------------------------+
|                                                             |                                             |
| [Gestion des utilisateurs](#utilisateurs-et-droits)         | [pingUsers](#pingusers)                     |
|                                                             | [setRole](#setrole)                         |
|                                                             | [registerKey](#registerkey)                 |
|                                                             | [delKey](#delkey)                           |
+-------------------------------------------------------------+---------------------------------------------+
| [Commandes admin](#commandes-de-niveau-admin)               | [resetCamelis](#resetcamelis)               |
|                                                             | [importCtx](#importctx)                     |
|                                                             | [importLis](#importlis)                     |
|                                                             | [importFile](#importfile)                   |
+-------------------------------------------------------------+---------------------------------------------+
| [Commandes collaborator](#commandes-de-niveau-collaborator) | [exportLis](#exportlis)                     |
|                                                             | [exportCtx](#exportctx)                     |
|                                                             | [delObjects](#delobjects)                   |
|                                                             | [delFeatureFromReq](#delfeaturefromreq)     |
|                                                             | [delFeatureFromOid](#delfeaturefromoid)     |
|                                                             | [hideFeature](#hidefeature)                 |
|                                                             | [createDeleteRule](#createdeleterule)       |
|                                                             | [removeRule](#removerule)                   |
+-------------------------------------------------------------+---------------------------------------------+
| [Commandes publisher](#commandes-de-niveau-publisher)       | [setFeaturesFromReq](#setfeaturesfromreq)   |
|                                                             | [setFeaturesFromOids](#setfeaturesfromoids) |
|                                                             | [createObject](#createobject)               |
|                                                             | [addAxiom](#addaxiom)                       |
|                                                             | [renameFeature](#renamefeature)             |
|                                                             | [createPasteRule](#createpasterule)         |
|                                                             | [upload](#upload)                           |
+-------------------------------------------------------------+---------------------------------------------+
| [Commandes reader](#commandes-de-niveau-reader)             | [intent](#intent)                           |
|                                                             | [extent](#extent)                           |
|                                                             | [getIncrements](#getincrements)             |
|                                                             | [getValuedFeatures](#getvaluedfeatures)     |
|                                                             | [listRules](#listrules)                     |
+-------------------------------------------------------------+---------------------------------------------+



Commandes générales
===================

ping
----

But : obtenir des informations minimales sur le serveur et le contexte.

Appel : <http://localhost:9999/ping/>

Réponse :
```xml
<pingResponse activationDate="2012-10-23T15:20:36+00:00"
              lastUpdate="none"
              nbObject="9"
              pid="29715"/>
```


Utilisateurs et droits
======================


`xml-lis-server` gère une liste d'utilisateurs. Un utilisateur est une
paire identifiant/rôle. Les identifiants sont des chaînes hexadécimales.
Les rôles sont au nombre de 5, listés ci-dessous par droits croissants :

- none : aucun droit
- reader : peut lire le contexte
- publisher : reader + peut faire des ajouts dans le contexte
- collaborator : publisher + peut faire des suppressions dans le contexte
- admin : collaborator + peut gérer les utilisateurs

Par défaut, un utilisateur de type `admin` est créé au lancement. Son
identifiant est précisé en paramètre lors du lancement de
`xml-lis-server`.

Note : il existe un sixième rôle : super_admin. Celui-ci n'est en fait
utilisé que par les couches hautes de Portalis : il correspond à un
utilisateur qui aurait des droits admin sur tous les contextes du
portail.

On notera de plus que chaque identifiant d'utilisateur expire au bout
d'un certain temps, par défaut trois heures, à l'exception de celui du
créateur.

Toutes les commandes suivantes, permettant d'agir sur les utilisateurs
sont de niveau Admin.


pingUsers
---------

But : obtenir la liste des utilisateurs.

Appel : <http://localhost:9999/pingUsers?userKey=a243F>

Réponse :
```xml
<pingUsersResponse status="ok"
                   lastUpdate="2014-05-21T12:46:20.989+00:00">
    <user key="a243F" role="admin" expires="never"/>
    <user key="987fed65cb" role="reader" expires="2014-05-21T15:46:20+00:00"/>
    <user key="abc123ef45" role="admin" expires="2014-05-21T15:46:20+00:00"/>
</pingUsersResponse>
```

Pour chaque utilisateur, on donne son identifiant, son rôle, et la date
d'expiration.


setRole
-------

But : changer les droits d'un utilisateur

Appel : <http://localhost:9999/setRole?adminKey=a243F&userKey=abc123ef45&role=collaborator>

Paramètres :

- adminKey : identifiant de l'admin qui effectue l'opération
- userKey : identifiant de l'utilisateur dont on change les droits
- role : le nouveau rôle

Réponse :
```xml
<setRoleResponse status="ok" lastUpdate="2014-05-21T13:20:54.743+00:00">
    <user key="abc123ef45" role="collaborator" expires="2014-05-21T16:21:46+00:00"/>
</setRoleResponse>
```


registerKey
-----------

But : enregistrer un nouvel utilisateur

Appel : <http://localhost:9999/registerKey?adminKey=a243F&userKey=def&role=reader&timeout=60>

Paramètres :

- adminKey : identifiant de l'admin qui effectue l'opération
- userKey : identifiant du nouvel utilisateur
- role : rôle du nouvel utilisateur
- timeout : délai d'expiration, en secondes (optionnel, par défaut : trois heures)

Réponse :
```xml
<registerKeyResponse status="ok" lastUpdate="2014-05-21T13:20:54.743+00:00">
    <user key="def" role="reader" expires="2014-05-21T13:26:02+00:00"/>
</registerKeyResponse>
```


delKey
------

But : supprimer un utilisateur

Appel : <http://localhost:9999/delKey?adminKey=a243F&userKey=987fed65cb>

Paramètres :

- adminKey : identifiant de l'admin qui effectue l'opération
- userKey : identifiant de l'utilisateur à supprimer

Réponse :
```xml
<delKeyResponse status="ok"
                lastUpdate="2014-05-21T13:20:54.743+00:00"/>
```


Autres commandes de niveau Admin
================================

resetCamelis
------------

But : faire une remise à zéro du contexte (on obtient un contexte vierge).

Appel : <http://localhost:9999/resetCamelis?userKey=a243F>

Paramètre :

- userKey : identifiant de l'utilisateur qui effectue l'opération

Réponse :
```xml
<resetCamelisResponse status="ok"
                      lastUpdate="2014-05-21T13:20:54.743+00:00"/>
```


importCtx
---------

But : importer un contexte au format ctx dans le contexte courant.

Appel : <http://localhost:9999/importCtx?userKey=a243F&ctxfile=planets.ctx>

Paramètres :

- userKey : identifiant de l'utilisateur qui effectue l'opération
- ctxfile : nom du fichier à importer. Il doit se trouver dans le
  répertoire du contexte

Réponse :
```xml
<importCtxResponse status="ok"
                   lastUpdate="2014-05-21T13:43:52.250+00:00"
                   contextName="planets"
                   ctxfile="planets.ctx"/>
```

importLis
---------

But : remplace le contexte actuel par un nouveau contexte (fichier
`.lis`).

Appel : <http://localhost:9999/importLis?userKey=a243F&lisfile=planets.lis>

Paramètres :

- userKey : identifiant de l'utilisateur qui effectue l'opération
- lisfile : nom de fichier du nouveau contexte. Il doit se trouver dans
  le répertoire du contexte

Réponse :
```xml
<importLisResponse status="ok"
                   lastUpdate="2014-05-21T13:47:17.949+00:00"
                   contextName="planets"
                   lisfile="planets.lis"/>
```


importFile
----------

But : importer un fichier dans le contexte courant.

Appel : <http://localhost:9999/importFile?userKey=a243F&file=ferre.bib&recursive=true&parts=true>

Paramètres :

- userKey : identifiant de l'utilisateur qui effectue l'opération
- file : nom de fichier à importer. Il doit se trouver dans le
  répertoire du contexte
- recursive : booléen (true/false) (optionnel, défaut : false)
- parts : booléen (true/false) (optionnel, défaut : false)

Réponse :
```xml
<importFileResponse status="ok"
                    lastUpdate="2014-05-21T13:48:29.143+00:00"/>
```


Commandes de niveau Collaborator
================================

exportLis
---------

But : récupérer le contexte (fichier `.lis`).

Appel : <http://localhost:9999/exportLis?userKey=a243F>

Paramètre :

- userKey : identifiant de l'utilisateur qui effectue l'opération

Réponse :
```xml
<exportLisResponse status="ok"
                   lastUpdate="2014-05-22T12:57:59.275+00:00"
                   data="hJW[...]QEA="/>
```

L'attribut data de la réponse contient le fichier `.lis` du contexte,
encodé en base64.


exportCtx
---------


But : récupérer le contexte (fichier `.ctx`).

Appel : <http://localhost:9999/exportCtx?userKey=a243F>

Paramètre :

- userKey : identifiant de l'utilisateur qui effectue l'opération

Réponse :
```xml
<exportLisResponse status="ok"
                   lastUpdate="2014-05-22T12:57:59.275+00:00"
                   data="c3J[...]Cg=="/>
```

L'attribut data de la réponse contient le fichier `.ctx` du contexte,
encodé en base64.

delObjects
----------

But : supprimer un ou plusieurs objets du contexte.

Appel : <http://localhost:9999/delObjects?userKey=a243F&oid=6&oid=1>

Paramètres :

- userKey : identifiant de l'utilisateur qui effectue l'opération
- oid (entier) : oid de l'objet à supprimer. Pour supprimer plusieurs
  objets, il sufffit de préciser plusieurs fois ce paramètre, comme dans
  l'exemple ci-dessus.

Réponse :
```xml
<delObjectsResponse status="ok" lastUpdate="2014-05-22T12:57:59.275+00:00"/>
```

delFeatureFromReq
-----------------

But : supprimer certaines features de la description des objets
sélectionnés par une requête.

Appel : <http://localhost:9999/delFeatureFromReq?userKey=a243F&wq=all&reqForOids=near&feature=satellite&feature=small>

Paramètres :

- userKey : identifiant de l'utilisateur qui effectue l'opération
- wq : la requête courante
- reqForOids : requête qui sélectionne les objets à mettre à jour
- feature : feature à supprimer de la description de ces objets. Pour
  supprimer plusieurs features, il sufffit de préciser plusieurs fois ce
  paramètre, comme dans l'exemple ci-dessus
- tree : booléen (true/false) permettant de retourner la liste des
  nouveaux incréments après la mise à jour (optionnel, défaut : false)

Réponse :
```xml
<delFeatureFromReqResponse status="ok"
                           lastUpdate="2014-05-22T13:02:26.983+00:00"/>
```

delFeatureFromOid
-----------------

But : supprimer certaines features de la description d'un ou plusieurs
objets.

Appel : <http://localhost:9999/delFeatureFromOid?userKey=a243F&wq=all&oid=3&feature=satellite&feature=small>

Paramètres :

- userKey : identifiant de l'utilisateur qui effectue l'opération
- wq : la requête courante
- oid (entier) : oid de l'objet à mettre à jour. Pour mettre à jour la
  description de plusieurs objets, il sufffit de préciser plusieurs fois
  ce paramètre
- feature : feature à supprimer de la description de ces objets. Pour
  supprimer plusieurs features, il sufffit de préciser plusieurs fois ce
  paramètre, comme dans l'exemple ci-dessus
- tree : booléen (true/false) permettant de retourner la liste des
  nouveaux incréments après la mise à jour (optionnel, défaut : false)

Réponse :
```xml
<delFeatureFromOidResponse status="ok"
                           lastUpdate="2014-05-22T13:15:42.057+00:00"/>
```

hideFeature
-----------

But : masquer une ou plusieurs features du contexte.

Appel : <http://localhost:9999/hideFeature?userKey=a243F&wq=all&feature=Distance&feature=Size>

Paramètres :

- userKey : identifiant de l'utilisateur qui effectue l'opération
- wq : la requête courante
- feature : feature à masquer. Pour masquer plusieurs features, il
  sufffit de préciser plusieurs fois ce paramètre, comme dans l'exemple
  ci-dessus.

Réponse :
```xml
<hideFeatureResponse status="ok" lastUpdate="2014-05-22T13:17:01.186+00:00">
    <increments>
        <increment name="Far" card="4"/>
        <increment name="Jovian" card="4"/>
        <increment name="satellite" card="4"/>
        <increment name="Near" card="3"/>
        <increment name="Small" card="3"/>
    </increments>
</hideFeatureResponse>
```

La réponse présente la liste des incréments de la requête courante. La
description xml des incréments est décrite dans la commande
[zoom](#zoom).

Il est important de noter que la commande hideFeature ne permet de
masquer ques des incréments intermédiaires, et non des feuilles de
l'arbre des incréments.

createDeleteRule
----------------

But : créer une règle de type `delete`

Appel : <http://localhost:9999/createDeleteRule?userKey=a243F&wq=satellite>

Paramètres :

- userKey : identifiant de l'utilisateur qui effectue l'opération
- wq : la requête courante

Réponse :
```xml
<createDeleteRuleResponse status="ok" lastUpdate="2014-05-03T13:25:13.402+00:00"/>
```

removeRule
----------

But : supprimer une règle

Appel : <http://localhost:9999/removeRule?userKey=a243F&ruleId=2>

Paramètres :

- userKey : identifiant de l'utilisateur qui effectue l'opération
- ruleId (entier) : le numéro identifiant la règle à supprimer. Celui-ci est
  donné par la commande [listRules](#listrules).

Réponse:
```xml
<removeRuleResponse status="ok" lastUpdate="2014-05-03T13:26:15.472+00:00"/>
```


Commandes de niveau Publisher
=============================

setFeaturesFromReq
------------------

But : ajouter certaines features à la description d'un ou plusieurs
objets sélectionnés par une requête.

Appel : <http://localhost:9999/setFeaturesFromReq?userKey=a243F&wq=all&reqForOids=big&feature=plop>

Paramètres :

- userKey : identifiant de l'utilisateur qui effectue l'opération
- wq : la requête courante
- reqForOids : requête qui sélectionne les objets à mettre à jour
- feature : feature à ajouter à la description de ces objets. Pour
  ajouter plusieurs features, il sufffit de préciser plusieurs fois ce
  paramètre.
- tree : booléen (true/false) permettant de retourner la liste des
  nouveaux incréments après la mise à jour (optionnel, défaut : false)

Réponse :
```xml
<setFeaturesFromReqResponse status="ok"
                            lastUpdate="2014-05-22T13:23:46.586+00:00"/>
```

setFeaturesFromOids
-------------------

But : ajouter certaines features à la description d'un ou plusieurs
objets mentionnés explicitement.

Appel : <http://localhost:9999/setFeaturesFromOids?userKey=a243F&wq=all&oid=10&feature=big&feature=small>

Paramètres :

- userKey : identifiant de l'utilisateur qui effectue l'opération
- wq : la requête courante
- oid (entier) : oid de l'objet à mettre à jour. Pour mettre à jour la
  description de plusieurs objets, il sufffit de préciser plusieurs fois
  ce paramètre.
- feature : feature à ajouter à la description de ces objets. Pour
  ajouter plusieurs features, il sufffit de préciser plusieurs fois ce
  paramètre.
- tree : booléen (true/false) permettant de retourner la liste des
  nouveaux incréments après la mise à jour (optionnel, défaut : false)

Réponse :
```xml
<setFeaturesFromOidsResponse status="ok"
                             lastUpdate="2014-05-22T13:33:00.291+00:00"/>
```

createObject
------------

But : créer un nouvel objet.

Appel : <http://localhost:9999/createObject?userKey=a243F&name=moon&feature=near>

Paramètres :

- userKey : identifiant de l'utilisateur qui effectue l'opération
- name : nom du nouvel objet (chaîne de caractères)
- feature : feature à ajouter à la description du nouvel objet. Pour
  ajouter plusieurs features, il sufffit de préciser plusieurs fois ce
  paramètre.
- picture : nom de fichier de l'image associée au nouvel objet. Le
  fichier doit se trouver dans le répertoire du contexte. Optionnel.


Réponse :
```xml
<createObjectResponse status="ok" lastUpdate="2014-05-22T13:36:14.889+00:00">
    <extent>
        <object oid="11" name="moon"/>
    </extent>
</createObjectResponse>
```

La réponse liste l'extension de la description du nouvel objet. La
description xml d'une extension est décrite dans la commande
[extent](#extent).


addAxiom
--------

But : ajouter un axiome au contexte.

Appel : <http://localhost:9999/addAxiom?userKey=a243F&premise=satellite&conclusion=Misc>

Paramètres :

- userKey : identifiant de l'utilisateur qui effectue l'opération
- premise (feature) : la prémisse de l'axiome
- conclusion (feature) : la conclusion de l'axiome

Réponse :
```xml
<addAxiomResponse status="ok"
                  lastUpdate="2014-05-22T13:41:59.965+00:00"/>
```


renameFeature
-------------

But : renommer une feature

Appel : <http://localhost:9999/renameFeature?userKey=a243F&feature=Size&newName=Bigness>

Paramètres :

- userKey : identifiant de l'utilisateur qui effectue l'opération
- feature : le nom de la feature à renommer
- newName : le nouveau nom

Réponse:
```xml
<renameFeatureResponse status="ok"
                       lastUpdate="2014-05-22T13:41:59.965+00:00"/>
```

createPasteRule
---------------

But : créer une règle de type `paste`

Appel : <http://localhost:9999/createPasteRule?userKey=a243F&update=riri&wq=Loulou>

Paramètres :

- userKey : identifiant de l'utilisateur qui effectue l'opération
- wq : la requête courante (premisse de la règle)
- update (feature) : la propriété à appliquer aux objets satisfaisant la prémisse

Réponse :
```xml
<createPasteRuleResponse status="ok" lastUpdate="2014-05-03T13:30:40.261+00:00"/>
```


upload
------

But : ajouter un fichier au contexte

Appel : il faut utiliser une requête POST sur
http://localhost:9999/upload. Cela se fait le plus souvent en utilisant
un formulaire html contenant un champ input de type file. On peut aussi
le faire en ligne de commande en utilisant curl :

```
curl --form userKey=a243F --form file=@/path/to/file http://localhost:9999/upload
```

Paramètres :

- userKey : identifiant de l'utilisateur qui effectue l'opération
- file : fichier à envoyer
- force : écraser le fichier si il est déjà présent (booléen, défaut : true)

Le fichier est placé sur le serveur dans le répertoire du contexte. Si
le fichier existe déjà, une erreur sera renvoyée, sauf si le paramètre
`force` est spécifié. Dans ce cas,le fichier sera remplacé si
l'utilisateur a des droits, non pas de niveau Publisher, mais de niveau
Collaborator.

Réponse :
```xml
<uploadResponse status="ok" lastUpdate="2014-05-03T13:25:13.402+00:00"/>
```


Commandes de niveau Reader
==========================


intent
------

But : obtenir l'intention d'un ou plusieurs d'objets.

Appel : <http://localhost:9999/intent?userKey=a243F&oid=1&oid=2>

Paramètres:
- userKey : identifiant de l'utilisateur qui effectue l'opération
- oid (entier) : oid de l'objet dont on cherche l'intention. Pour obtenir
  l'intention d'un ensemble d'objets, il sufffit de préciser plusieurs
  fois ce paramètre.

Réponse :
```xml
<intentResponse status="ok" lastUpdate="2014-05-22T13:46:05.575+00:00"
                intentAsQuery="satellite and Size and Distance >
    <intent>
        <feature name="satellite"/>
        <feature name="Size"/>
        <feature name="Distance"/>
    </intent>
</intentResponse>
```

Une intention est donnée sous deux formes : la requête correspondante,
et la liste des features qui la composent.


extent
------

But : obtenir l'extension d'une requête.

Appel : <http://localhost:9999/extent?userKey=abc123ef45&wq=satellite%20and%20Near>

Paramètres :

- userKey : identifiant de l'utilisateur qui effectue l'opération
- wq : requête dont on cherche l'intention. Les caractères spéciaux
  de la requête (espaces, dans l'exemple ci-dessus) ont été URL-encodés.
- pageSize (entier) : nombre maximum d'objets à rendre. Optionnel, défaut :
  cardinal de l'extension.
- page (entier) : numéro de la page de résultats à rendre. Optionnel, défaut : 1.

Réponse :
```xml
<extentResponse status="ok" lastUpdate="2014-05-22T13:46:05.575+00:00" nbObjects="2">
    <extent>
        <object oid="1" picture="/tmp/planets/images/earth.jpg" name="earth"/>
        <object oid="3" name="mars"/>
    </extent>
</extentResponse>
```

Une extension est composée de la liste de ses objets. Chaque objet est
représenté par un triplet <oid, image associée, nom> ou bien par un
doublet <oid, nom> si l'objet n'a pas d'image associée.


getIncrements
-------------

But : obtenir la liste des incréments d'une requête

Appel : <http://localhost:9999/getIncrements?wq=all&feature=Size&userKey=a243F>

Paramètres :

- userKey : identifiant de l'utilisateur qui effectue l'opération
- wq : requête courante
- feature : feature dont on veut lister les sous-incréments
- showLeaf : booléen ; indiquer l'absence de sous-incréments. Optionnel,
  défaut : false.
- pageSize (entier) : nombre maximum de résultats à rendre. Optionnel, défaut :
  tout.
- page (entier) : numéro de la page de résultats à rendre. Optionnel, défaut : 1.

Réponse :
```xml
<getIncrementsResponse status="ok"
              lastUpdate="2014-05-23T05:32:46.720+00:00"
              newWq="Size" nbObjects="10" nbIncrs="2">
    <increments>
        <increment name="Small" card="5" isLeaf="true"/>
        <increment name="Jovian" card="4" isLeaf="false"/>
    </increments>
</getIncrementsResponse>
```

Chaque incrément est caractérisé par son nom et son cardinal. La
propriété `isLeaf` n'est présente que si elle a été demandée en
utilisant `showLeaf=true` lors de l'appel.


getValuedFeatures
-----------------

But : récupérer la valeur d'un ou plusieurs attribut(s) valué(s) pour un
objet ou un ensemble d'objets.

Appel : <http://localhost:9999/getValuedFeatures?userKey=a243F&oid=1&oid=2&featname=foo&featname=bar>

Paramètres :

- userKey : identifiant de l'utilisateur qui effectue l'opération
- featname : nom de l'attribut valué. Pour rechercher les valeurs de
  plusieurs attributs valués, il suffit de spécifier ce paramètre
  plusieurs fois.
- oid (entier) : objet dont on recherche la valeur de l'attribut valué. Pour un
  ensemble d'objets, il suffit de spécifier ce paramètre plusieurs fois.

Réponse :
```xml
<getValuedFeaturesResponse status="ok" lastUpdate="2014-05-23T09:15:07.788+00:00">
    <object oid="1">
        <property name="foo" value="toto"/>
        <property name="bar" value="42"/>
        <property name="bar" value="12"/>
    </object>
    <object oid="2">
        <property name="foo" value="tata"/>
        <property name="bar" value="1618"/>
    </object>
</getValuedFeaturesResponse>
```

La réponse est donc la liste des objets demandés. À chaque objet est
associée la liste des attributs valués demandés, la valeur étant séparée
du nom de l'attribut.


getResource
-----------

But : récupérer un fichier du contexte

Appel : <http://localhost:9999/getResource?userKey=a243F&file=mpthreetest.mp3>

Paramètres :

- userKey : identifiant de l'utilisateur qui effectue l'opération
- file : nom du fichier à récupérer

Réponse : le fichier est directement renvoyé, comme si on le téléchargeait.


getThumbnail
------------

But : récupérer une miniature pour une image du contexte

Appel : <http://localhost:9999/getThumbnail?userKey=a243F&file=images/earth.jpg&width=48&height=48>

Paramètres :

- userKey : identifiant de l'utilisateur qui effectue l'opération
- file : nom du fichier image dont on veut une miniature
- width (entier) : largeur de la miniature, en pixels. Optionnel, défaut : 64.
- height (entier) : hauteur de la miniature, en pixels. Optionnel, défaut : 64.

Réponse : le fichier est directement renvoyé, comme si on le téléchargeait.


listRules
---------

But : afficher la liste des règles du contexte

Appel : <http://localhost:9999/listRules?userKey=a243F>

Paramètres :

- userKey : identifiant de l'utilisateur qui effectue l'opération

Réponse :
```xml
<listRulesResponse status="ok" lastUpdate="2014-07-02T12:46:26.006+00:00">
    <rule ruleId="0" kind="paste intr" query="Flip" update="Flap"/>
    <rule ruleId="1" kind="paste" query="Hip" update="Hop"/>
    <rule ruleId="2" kind="delete" query="Plop"/>
</listRulesResponse>
```

La réponse contient la liste des règles. Chaque règle est caractérisée
par son type, la requête à laquelle elle s'applique et, dans le case de
règles de type paste, la mise à jour à appliquer.


Erreurs
=======

Dans tous les exemples mentionnés précédemment, l'exécution de la
commande se déroule correctement. Ainsi, toutes les réponses contiennent
l'attribut xml `status="ok"`.

Or, il peut se produire des erreurs. Dans ce cas de figure, on obtient
des réponses avec l'attribut xml `status="error"` ainsi qu'un message
d'erreur. Par exemple, l'appel à <http://localhost:9999/load> produit le
message suivant :

```xml
<loadResponse status="error">
    <message>parameter userKey is missing</message>
</loadResponse>
```

On peut retrouver la liste des messages d'erreurs dans le code de
`xml-lis-server` en regardant la définition de `xml_of_exn` dans le
fichier `main.ml`.
