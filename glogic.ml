open Logtoken
open Logic

let toks_bot = [Nat 0]
let toks_top = [Interro]

module MyInt =
  Intpow.Make
    (struct
      let toks_unit = []
     end)

module MyNum = Numpow.Make

module MyInterval = Openinterval.Brackets

module MyEmpty =
  Empty.Make
    (struct
      let toks_bot = toks_bot
     end)

module MyInsert =
  Insert.Make
    (struct
      let toks_a1 = []
     end)

module MyTop =
  Top.Make
    (struct
      let toks_top = toks_top
      let feat_top = false
      let gen_top = false
     end)

module MyProd =
  Prod.Make
    (struct
      let toks_pre = []
      let toks_sep = [PP_tilda]
      let toks_suf = []
      let toks_bot = toks_bot
     end)

module MyPrelist =
  Prelist.Make
    (struct
      let toks_top = toks_top
      let toks_nil = []
      let toks_sep = [PP_tilda]
      let toks_bot = toks_bot
      let feat_top = true
     end)

module ValInt = MyInterval (MyNum)

module PrefixDate =
  Prefix.Make
    (struct
      let toks = [Ident "date"; PP_tilda]
     end)
module ValDate = PrefixDate (MyInterval (Date.Make))

module PrefixTime =
  Prefix.Make
    (struct
      let toks = [Ident "time"; PP_tilda]
     end)
module ValTime = PrefixTime (MyInterval (Time.Make))

module PrefixPerm =
  Prefix.Make
    (struct
      let toks = [Ident "perm"; PP_tilda]
     end)
module ValPerm = PrefixPerm (MyInterval (Permissions.Make))

module PrefixMLType =
  Prefix.Make
    (struct
      let toks = [Ident "mltype"; PP_tilda]
     end)
module ValMLType = PrefixMLType (Mltype.Make)

module PrefixJavaType =
  Prefix.Make
    (struct
      let toks = [Ident "java"; PP_tilda]
     end)
module ValJavaType = PrefixJavaType (Javatype.Make)

module PrefixMercuryType =
  Prefix.Make
    (struct
      let toks = [Ident "mercury"; PP_tilda]
     end)
module ValMercuryType = PrefixMercuryType (Mercurytype.Make)

module PrefixPregroupType =
  Prefix.Make
    (struct
      let toks = [Ident "pgtype"; PP_tilda]
     end)
module ValPregroupType = PrefixPregroupType (Pregrouptype.Make)

module ValSubstring =
  Substring.Verb
    (struct
      let normalize s = String.lowercase s
      let normalize_is s = s
      let words s = []
      (*      let words s = List.map (fun x -> (String.length x >= 3, x)) (Str.split (Str.regexp "[ ,;:()]+") s) *)
      let prefixes s = []
      let suffixes s = []
     end)

module SubstringPath =
  Substring.Verb
    (struct
      let normalize s = String.lowercase s
      let normalize_is s = s
      let words _ = []
      let prefixes s =
        let _, l =
          Common.fold_while
            (fun (path, l) ->
              let path' = Filename.dirname path in
              if path' = path
              then None
              else Some (path', (true, path) :: l))
            (s, []) in
        l
      let suffixes _ = []
     end)
module PrefixPath =
  Prefix.Make
    (struct
      let toks = [Ident "path"; PP_tilda]
     end)
module ValPath = PrefixPath (SubstringPath)

module ValAttr =
  Attr.Make
    (struct
      let toks_top = None
      let names x = not (List.mem x ["and"; "or"; "not"; "except"])
     end)

module Val1 =
  MyInsert (ValDate) (
    MyInsert (ValTime) (
      MyInsert (ValPath) (
        MyInsert (ValPerm) (
          MyInsert (ValMLType) (
            MyInsert (ValJavaType) (
              MyInsert (ValMercuryType) (
                MyInsert (ValPregroupType) (
                  MyInsert (ValAttr)
                    (MyEmpty)))))))))

module Val =
  MyInsert (ValInt) (
    MyInsert (ValSubstring)
      (Val1))

module Descr = MyTop (MyProd (Val1) (MyPrelist (Val)))

module CDescr = CGeneric.Make (Descr)


module Dir = Dir.Make
  (struct
    let filter path =
      try
        match (Unix.lstat path).Unix.st_kind with
          | Unix.S_DIR -> true
          | Unix.S_REG -> true
          | _ -> false
      with _ -> false
  end)

module Url =
  Sglobj.Make
    (struct
      let filter url = Url.is_local url || Url.is_on_web url
      let may_have_changed_since = Url.may_have_changed_since
      let get_obj url =
        let url_low = String.lowercase url in
        let c sufs = List.exists (Filename.check_suffix url_low) sufs in
        let lf = Url.fields_of_url "source" [] url in
        let pre =
          if c [".jpg"; ".jpeg"; ".png"; ".gif"; ".bmp"; ".tif"; ".tiff"]
          then Source.Picture (url, Url.preview_of_url url)
          else Source.String (Url.preview_of_url url) in
        let args =
          let mime, role =
            if c [".jpg"; ".jpeg"] then "image/jpeg", "image"
            else if c [".png"] then "image/png", "image"
            else if c [".gif"] then "image/gif", "image"
            else if c [".bmp"] then "image/bmp", "image"
            else if c [".tif"; ".tiff"] then "image/tiff", "image"
            else if c [".mp3"] then "audio/mpeg", "MP3"
            else if c [".ogg"] then "audio/ogg", "Ogg Vorbis"
            else if c [".flac"] then "audio/x-flac", "Flac"
            else if c [".mpc"] then "audio/x-musepack", "Musepack"
            else if c [".wav"] then "audio/wav", "WAV"
            else if c [".mov"] then "video/mov", "movie"
            else if c [".html"; ".htm"] then "text/html", "web page"
            else if c [".pdf"] then "application/pdf", "PDF"
            else if c [".lis"] then "application/lis", "LIS context"
            else if c [".ctx"] then "text/ctx", "LIS context"
            else "text", "file" in
          { Source.mime = Url.mime url mime; Source.role = role; Source.location = url } :: Url.args_of_url url in
        (lf, pre, args)
     end)

module Src =
  Union.Make (Ctx.Make) (
    Union.Make (Agenda.Make) (
      Union.Make (Tnt.Make) (
        Union.Make (Mercury.Make) (
          Union.Make (Oiseaux_region.Make) (
            Union.Make (Oiseaux_pays.Make) (
              Union.Make (Oiseaux_fiche.Make) (
                Union.Make (Dblp_search.Make) (
                  Union.Make (Dblp_rec.Make) (
                    Union.Make (Bibtex.Make) (
                      Union.Make (Bookmarks.Make) (
                        Union.Make (Nsmail.Make) (
                          Union.Make (Audio.Make) (
                            Union.Make (Jpeg.Make) (
                              Union.Make (Csv.Make) (
                                Union.Make (Mli.Make) (
                                  Union.Make (Java.Make) (
                                    Union.Make (Dir) (
                                      Url))))))))))))))))))


module WllBibtex = Wbibtex.Make

module WllBibitems_csv = Wbibitems_csv.Make
  (struct
    let fields = [["type"]; ["year"]; ["authors"]; ["chapter"; "title"]; ["journal"; "booktitle"; "title"]]
   end)

module WllImages = Urllist.Make
  (struct
    let kinds =
      match Sys.os_type with
        | "Unix" -> [("gqv", "Slide show")]
        | "Cygwin" -> [("txt", "Slide show")]
        | _ -> []
    let url_arg = "image"
    let key_arg = None
    let mime =
      match Sys.os_type with
        | "Unix" -> "list/image/gqv"
        | "Cygwin" -> "list/image"
        | _ -> "list/image"
    let role = "Diaporama"
    let head =
      match Sys.os_type with
        | "Unix" -> "#GQview collection\n#created with GQview version 1.0.2\n#geometry: 527 216 440 450\n"
        | "Cygwin" -> ""
        | _ -> ""
    let tail =
      match Sys.os_type with
        | "Unix" -> "#end\n"
        | "Cygwin" -> ""
        | _ -> ""
    let item file =
      match Sys.os_type with
        | "Unix" -> "\"" ^ file ^ "\"\n"
        | "Cygwin" -> Common.cyg2win file ^ "\n"
        | _ -> file ^ "\n"
    let url_compare = compare
   end)

module WllMp3 = Urllist.Make
  (struct
    let kinds = [("m3u", "Playlist")]
    let url_arg = "audio/mpeg"
    let key_arg = None
    let mime = "list/audio/m3u"
    let role = "list"
    let path_of_listname listname =
      match Sys.os_type with
        | "Unix" -> listname
        | "Cygwin" -> listname
        | _ -> listname
    let head = ""
    let tail = ""
    let item url = Source.localize_path url ^ "\n"
    let url_compare = compare
   end)

module Wll =
  Wunion.Make
    (Wunion.Make
       (WllBibtex)
       (WllBibitems_csv))
    (Wunion.Make
       (WllImages)
       (WllMp3))

