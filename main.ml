open Nethttpd_types
open Nethttpd_services
open Nethttpd_engine
open Utils
open StdLabels
open Mimetypes
open Users

module Context = Context.Make(Glogic.CDescr)(Glogic.Src)(Glogic.Wll)
module Ext = Context.Ext
module Query = Context.Root.Query


(* -------------------------------------------------------------- *)
(* Globals *)
let port = ref 0

let activation_date = ref (rfc3339_now ())
let last_update = ref "none"

let logfile = ref "/tmp/portalis.log"
let ignore_rights = ref false

let datadir = ref ""
let context_name = ref ""    (* initialized after CLI args are parsed *)


(* -------------------------------------------------------------- *)
(* exception handling *)
exception No_context
exception Context_out_of_bounds
exception Bad_rule_index of int
(* in module Users:
   - No_such_role of string
   - Not_enough_rights
   - No_such_key of string
   - Expired_key of string
   - Registered_key of string
 *)

type param_error = Missing_parameter of string | Empty_parameter of string
exception Parameter_error of param_error list

let xml_of_exn = fun e ->
  let xml_msg msg = [Xml.Element ("message", [], [Xml.PCData msg])] in
  match e with
  | No_context -> xml_msg "No context is loaded."
  | Context_out_of_bounds -> xml_msg "This is not a valid context page (out of bounds)."
  | Bad_rule_index i -> xml_msg ("No rule correspond to ruleId " ^ (string_of_int i) ^ ".")
  | No_such_role str -> xml_msg (str ^ " is not a valid role name.")
  | Not_enough_rights -> xml_msg "Not enough rights"
  | No_such_key str -> xml_msg ("Key " ^ str ^ " is not registered for this service.")
  | Expired_key str -> xml_msg ("Key " ^ str ^ " has expired.")
  | Registered_key str -> xml_msg ("Key " ^ str ^ " is already registered.")
  | Parameter_error errors ->
    let f = fun err -> match err with
      | Missing_parameter str -> xml_msg ("parameter " ^ str ^ " is missing")
      | Empty_parameter str -> xml_msg ("parameter " ^ str ^ " is empty")
    in
    List.flatten (List.map f errors)
  | No_such_file str -> xml_msg ("File " ^ str ^ " was not found on the server.")
  | File_already_exists str -> xml_msg ("File " ^ str ^ " already exists on the server.")
  | Todo str -> xml_msg ("TODO: implement " ^ str ^ " command.")
  | _ -> xml_msg (Printexc.to_string e)



(* -------------------------------------------------------------- *)
(* Common sanity checks *)

let check_param = fun param (cgi: Netcgi.cgi_activation) ->
  if not (cgi # argument_exists param) then
    Some (Missing_parameter param)
  else if (cgi # argument_value param) = "" then
    Some (Empty_parameter param)
  else
    None

let assert_params = fun params (cgi: Netcgi.cgi_activation) ->
  let checks = List.map (fun p -> check_param p cgi) params in
  let errors = List.filter (fun x -> is_some x) checks in
  let errors' = List.map val_of_option errors in
  if errors' <> [] then raise (Parameter_error errors')



(* -------------------------------------------------------------- *)
(* Helper functions for Camelis *)

let rec wait_for_camelis_updates () =
  (* we'd like to use Unix.sleep, but its minimum is a 1 second sleep,
   * which is way too long here . So we use Unix.select in a
   * non-standard way to pause for 10 ms. *)
  let _ = Unix.select [] [] [] 0.01 in
  let n = Context.nb_updates () in
  if n > 0
  then wait_for_camelis_updates ()
  else ()


let awaken = fun f ->
  wait_for_camelis_updates ();
  ignore (f ())


let save_context = fun () ->
  awaken (fun () -> Context.save $ !datadir // !context_name);
  last_update := rfc3339_now ();
  logline !logfile ("Context " ^ !context_name ^ " saved.")



let nb_objects = fun () ->
  Ext.cardinal (Context.extent Query.top)


(* Retrieve file associated with some oid, in case the file was imported
   into the context *)
let file_of_oid = fun oid ->
  match Context.origin_obj oid with
  | Context.Object.Internal (pre, args) -> None
  | Context.Object.External (s, r) -> Some (Context.Src.url s)


let descr_of_oid = fun oid ->
  let preview = match Context.origin_obj oid with
    | Context.Object.Internal (pre, args) -> pre
    | Context.Object.External (s, r) -> Context.Src.preview s r
  in
  match preview with
    | Source.String d ->
      let filename = file_of_oid oid in
      if (is_some filename) then
        ["file", string_of_option filename; "name", xml_string d]
      else
        ["name", xml_string d]
    | Source.Picture (f, d) ->
      ["picture", xml_string f; "name", xml_string d]


let string_of_feat f = Syntax.stringizer (Context.print f)

let string_of_query q = Syntax.stringizer (Context.print_query q)

let string_of_update u = Syntax.stringizer (Context.Root.Update.print u)

let parse_of_string p s =
  let (__strm: _ Stream.t) = Syntax.from_string s in
  let x = try p __strm
    with
    | Stream.Failure ->
      raise (Stream.Error "Syntax error: unexpected beginning") in
  let _ = try Stream.empty __strm
    with
    | Stream.Failure -> raise (Stream.Error "Syntax error: unexpected end")
  in x

let feat_of_string = parse_of_string Context.parse

let query_of_string = parse_of_string Context.parse_query

let update_of_string = parse_of_string Context.parse_update

(* Given a working query 'wq', return the list of increments (with their
   cardinal), the local objects and the extent of feature 'feat' *)
let ls' = fun wq feat ->
  let links_options = [] in
  let view = Context.links wq links_options feat in
  let incrs = List.fold_left
    ~f:(fun res propr ->
      let logs, supp = propr.Context.propr_logs, propr.Context.propr_supp in
      List.fold_left
        ~f:(fun res' log -> (log, supp) :: res')
        ~init:res
        logs
    )
    ~init:[]
    view.Context.proprs in
  let sort_by_count (feat1, card1) (feat2, card2) =
    Common.compare_pair (compare, Query.compare) (card2, feat1) (card1, feat2) in
  let cmp = sort_by_count in
  let incrs2 = List.sort ~cmp incrs in
  let incrs3 = List.map (fun (log, supp) -> (string_of_query log, supp)) incrs2 in
  (incrs3, view.Context.locals, view.Context.extent)

let ls wq feat = ls' (query_of_string wq) (query_of_string feat)



(* -------------------------------------------------------------- *)
(* conversion to Xml of Camelis objects *)

let xml_of_feat = fun f ->
  Xml.Element ("feature", ["name", xml_string $ string_of_feat f], [])

let xml_of_oid = fun oid ->
  Xml.Element ("object",
               ("oid", string_of_int oid) :: (descr_of_oid oid),
               [])

let xml_of_ext = fun e ->
  Xml.Element ("extent", [],
               List.map ~f:xml_of_oid (Ext.elements e))

let xml_of_incr = fun (i, card) ->
  Xml.Element ("increment", [ "name", xml_string i;
                              "card", string_of_int card
                            ], [])

let xml_of_incr' = fun (i, card, isLeaf) ->
  match isLeaf with
  | None -> xml_of_incr (i, card)
  | Some b -> Xml.Element ("increment", [ "name", xml_string i;
                                          "card", string_of_int card;
                                          "isLeaf", string_of_bool b;
                            ], [])


(* -------------------------------------------------------------- *)
(* Helper function for retriving context resources *)

let get_real_filename = fun name ->
  (* we have 4 cases:
   * 1. it is a url, return it as is
   * 2. it is a local file, in which case:
   *    2a. it is relative to datadir (does not start with a '/')
   *    2b. it is an absolute path on the filesystem (yes, sometimes
   *        camelis changes a relative path to an absolute one; for
   *        now, this is how we deal with it)
   *)
  if name.[0] == '/'
  then
    (* case 2b or 2c *)
    if (starts_with name !datadir) && (Sys.file_exists name) then name
    else raise (No_such_file name)
  else
    (* case 1 or 2a, so check if a file exists *)
    let candidate = !datadir // name in
    if (Sys.file_exists candidate) then candidate else name



(* -------------------------------------------------------------- *)
(* Helper functions for building xml answers *)

let xml_ok = fun ?(attrs = []) ?(children = []) tag ->
  let attrs' = ("status", "ok") :: ("lastUpdate", !last_update) :: attrs in
  Xml.Element (tag, attrs', children)


let xml_error = fun tag xml_msg ->
  Xml.Element (tag,
               ["status", "error"],
               xml_msg)



(* -------------------------------------------------------------- *)
(* Compute and send result to HTTP queries *)

let ping_service = fun tag cgi ->
  let attrs = [ "activationDate", !activation_date;
                "nbObject", string_of_int $ nb_objects ();
                "pid", string_of_int $ Unix.getpid ();
              ] in
  xml_ok tag ~attrs


let ping_users_service = fun tag (cgi:Netcgi.cgi_activation) ->
  let xml_users = List.map xml_of_user (user_list ()) in
  xml_ok tag ~children:xml_users


let set_role_service = fun tag (cgi: Netcgi.cgi_activation) ->
  let admin_key = cgi # argument_value "adminKey" in
  let user_key = cgi # argument_value "userKey" in
  let new_role_str = cgi # argument_value "role" in
  let _ = assert_rights admin_key Admin in
  let new_role = role_of_string new_role_str in
  let _ = update_user_role user_key new_role in
  xml_ok tag ~children: [xml_of_key user_key]


let register_key_service = fun tag (cgi: Netcgi.cgi_activation) ->
  let admin_key = cgi # argument_value "adminKey" in
  let user_key = cgi # argument_value "userKey" in
  let timeout = if cgi # argument_exists "timeout" then
      float_of_string (cgi # argument_value "timeout")
    else
      default_key_timeout in
  let _ = assert_new_key user_key in
  let new_role_str = cgi # argument_value "role" in
  let _ = assert_rights admin_key Admin in
  let new_role = role_of_string new_role_str in
  let _ = add_user user_key new_role (Some timeout) in
  xml_ok tag ~children: [xml_of_key user_key]


let delkey_service = fun tag (cgi: Netcgi.cgi_activation) ->
  let admin_key = cgi # argument_value "adminKey" in
  let user_key = cgi # argument_value "userKey" in
  let _ = assert_rights admin_key Admin in
  let _ = remove_user user_key in
  xml_ok tag


(* -------------------------------------------------------------- *)
(* Admin commands *)

let reset_context_service = fun tag (cgi: Netcgi.cgi_activation) ->
  let _ = Context.init ~log:false !context_name in
  xml_ok tag

let import_context_service = fun tag (cgi: Netcgi.cgi_activation) ->
  let ctxfile = cgi # argument_value "ctxfile" in
  let _ = assert (Filename.check_suffix ctxfile ".ctx") in
  let real_ctxfile = get_real_filename ctxfile in
  let _ = assert_file_exists real_ctxfile in
  let basename = Filename.chop_extension real_ctxfile in
  (* import in current context *)
  let options = [`Axioms; `Features; `Sources; `Extrinsic; `Wells; `Rules; `Actions] in
  let _ = awaken (fun () -> Context.import options basename) in
  (* save as a LIS file *)
  let _ = save_context () in
  (* cheerful answer to send *)
  xml_ok tag ~attrs:["contextName", !context_name;
                     "ctxfile", ctxfile]

let import_lis_service = fun tag (cgi: Netcgi.cgi_activation) ->
  let lisfile = cgi # argument_value "lisfile" in
  let _ = assert (Filename.check_suffix lisfile ".lis") in
  let real_lisfile = get_real_filename lisfile in
  let _ = assert_file_exists real_lisfile in
  (* update context name, then load from lisfile *)
  let _ = context_name := Filename.chop_extension lisfile in
  let _ = Context.load (Filename.chop_extension real_lisfile) in
  (* done! *)
  xml_ok tag ~attrs:["contextName", !context_name;
                     "lisfile", lisfile]

let import_file_service = fun tag (cgi: Netcgi.cgi_activation) ->
  let basename = cgi # argument_value "file" in
  let recursive = if cgi # argument_value "recursive" = "true" then [`Recursive] else [] in
  let parts = if cgi # argument_value "parts" = "true" then [`Parts] else [] in
  let file = !datadir // basename in
  let _ = assert_file_exists file in
  let options = recursive @ parts in
  (* TODO: allow use of url instead of Filename. *)
  let _ = Context.init_source file options Context.Object.Descr.empty in
  xml_ok tag


(* -------------------------------------------------------------- *)
(* Collaborator commands *)

let export_lis_service = fun tag (cgi: Netcgi.cgi_activation) ->
  let lisfile = !datadir // !context_name ^ ".lis" in
  let data = Netencoding.Base64.encode (string_of_file lisfile) in
  xml_ok tag ~attrs:["data", data]


let export_ctx_service = fun tag (cgi: Netcgi.cgi_activation) ->
  let is_partial = (cgi # argument_exists "wq") in
  let q = if is_partial
    then query_of_string (cgi # argument_value "wq")
    else Query.top in
  let ctxfile = Filename.temp_file !context_name "-export.ctx" in
  let dbname = Filename.chop_extension ctxfile in
  let all_options = [`Axioms; `Features; `Sources; `Extrinsic; `Wells;
                     `Rules; `Actions] in
  let _ = Context.export q all_options dbname in
  let data = Netencoding.Base64.encode (string_of_file ctxfile) in
  let _ = Sys.remove ctxfile in
  xml_ok tag ~attrs:["data", data]


let del_objects_service = fun tag (cgi: Netcgi.cgi_activation) ->
  let oids = List.map (fun x -> int_of_string x#value) (cgi # multiple_argument "oid") in
  let ext_oids = Ext.union_r (List.map Ext.singleton oids) in
  let del_oid oid = try Context.update_obj oid Context.Rem with _ -> () in
  let del_source = fun url () -> Context.final_source url in
  let _ = awaken (fun () -> Ext.iter del_oid ext_oids) in
  let _ = awaken (fun () -> Context.fold_sources_under del_source () ext_oids) in
  let _ = wait_for_camelis_updates () in
  xml_ok tag


let del_feature_from_req_service = fun tag (cgi: Netcgi.cgi_activation) ->
  let wq = url_decode $ cgi # argument_value "wq" in
  let features = List.map (fun x -> url_decode $ "not " ^ x # value) (cgi # multiple_argument "feature") in
  let req_for_oids = url_decode $ cgi # argument_value "reqForOids" in
  let get_feat_tree = cgi # argument_value "tree" in
  (* Retrieve objects and update their descriptions *)
  let _incrs, _locals, ext = ls req_for_oids "all" in
  let u = update_of_string (String.concat " and " features) in
  let _ = Ext.iter (fun oid -> Context.update_obj oid (Context.Extr u)) ext in
  (* Show feature tree if asked for *)
  let xml_incrs = if get_feat_tree = "true" then
      let incrs, _locals, _ext = ls wq "all" in
      [Xml.Element ("increments", [], List.map xml_of_incr incrs)]
    else
      [] in
  (* send result *)
  xml_ok tag ~children:xml_incrs


let del_feature_from_oid_service = fun tag (cgi: Netcgi.cgi_activation) ->
  let wq = url_decode $ cgi # argument_value "wq" in
  let features = List.map (fun x -> url_decode $ "not " ^ x # value) (cgi # multiple_argument "feature") in
  let oids = List.map (fun x -> int_of_string x#value) (cgi # multiple_argument "oid") in
  let get_feat_tree = cgi # argument_value "tree" in
  let u = update_of_string (String.concat " and " features) in
  let _ = List.iter (fun oid -> Context.update_obj oid (Context.Extr u)) oids in
  let xml_incrs = if get_feat_tree = "true" then
      let incrs, _locals, _ext = ls wq "all" in
      [Xml.Element ("increments", [], List.map xml_of_incr incrs)]
    else
      [] in
  xml_ok tag ~children:xml_incrs


let hide_feature_service = fun tag (cgi: Netcgi.cgi_activation) ->
  let wq = url_decode $ cgi # argument_value "wq" in
  let features = List.map (fun x -> url_decode x#value) (cgi # multiple_argument "feature") in
  let delete_feat f = Context.del_feature (feat_of_string f) in
  let _ = List.iter delete_feat features in
  let incrs, _locals, ext = ls wq "all" in
  let xml_incrs = Xml.Element ("increments", [], List.map xml_of_incr incrs) in
  xml_ok tag ~children:[xml_incrs]




(* -------------------------------------------------------------- *)
(* Publisher commands *)
let set_features_from_req_service = fun tag (cgi: Netcgi.cgi_activation) ->
  let wq = url_decode $ cgi # argument_value "wq" in
  let features = List.map (fun x -> url_decode x#value) (cgi # multiple_argument "feature") in
  let req_for_oids = url_decode $ cgi # argument_value "reqForOids" in
  let get_feat_tree = cgi # argument_value "tree" in
  (* Retrieve objects and update their descriptions *)
  let _incrs, _locals, ext = ls req_for_oids "all" in
  let u = update_of_string (String.concat " and " features) in
  let _ = Ext.iter (fun oid -> Context.update_obj oid (Context.Extr u)) ext in
  (* Show feature tree if asked for *)
  let xml_incrs = if get_feat_tree = "true" then
      let incrs, _locals, _ext = ls wq "all" in
      [Xml.Element ("increments", [], List.map xml_of_incr incrs)]
    else
      [] in
  (* send result *)
  xml_ok tag ~children:xml_incrs


let set_features_from_oids_service = fun tag (cgi: Netcgi.cgi_activation) ->
  let wq = url_decode $ cgi # argument_value "wq" in
  let features = List.map (fun x -> url_decode x#value) (cgi # multiple_argument "feature") in
  let oids = List.map (fun x -> int_of_string x#value) (cgi # multiple_argument "oid") in
  let get_feat_tree = cgi # argument_value "tree" in

  let u = update_of_string (String.concat " and " features) in
  let _ = List.iter (fun oid -> Context.update_obj oid (Context.Extr u)) oids in

  let xml_incrs = if get_feat_tree = "true" then
      let incrs, _locals, _ext = ls wq "all" in
      [Xml.Element ("increments", [], List.map xml_of_incr incrs)]
    else
      [] in
  xml_ok tag ~children:xml_incrs



let create_object_service = fun tag (cgi: Netcgi.cgi_activation) ->
  let object_name = url_decode $ cgi # argument_value "name" in
  let features = List.map (fun x -> url_decode x#value) (cgi # multiple_argument "feature") in
  let qdescr = query_of_string (String.concat " and " features) in
  let pre = if not (cgi # argument_exists "picture")
    then Source.String object_name
    else let picture = !datadir // (url_decode $ cgi # argument_value "picture") in
         Source.Picture (picture, object_name) in
  let args = [] in
  let d = Context.Object.Descr.of_query qdescr in
  let u = update_of_string "all" in
  let _ = Context.make_obj pre args d u in
  let _ = wait_for_camelis_updates () in
  let xml_extent = xml_of_ext (Context.extent qdescr) in
  xml_ok tag ~children:[xml_extent]


let add_features_service = todo_service


let add_axiom_service = fun tag (cgi:Netcgi.cgi_activation) ->
  let premise = feat_of_string (url_decode $ cgi # argument_value "premise") in
  let concl_str = String.capitalize (url_decode $ cgi # argument_value "conclusion") in
  let conclusion = feat_of_string concl_str  in
  let _ = Context.add_axiom premise conclusion in
  let _ = awaken (fun () -> Context.add_feature conclusion) in
  let _ = wait_for_camelis_updates () in
  let _incrs, _locals, _ext = ls concl_str "all" in
  xml_ok tag


let rename_feature_service = fun tag (cgi: Netcgi.cgi_activation) ->
  let orig = cgi # argument_value "feature" in
  let dest = cgi # argument_value "newName" in
  let _ = Context.replace_feature (feat_of_string orig) (feat_of_string dest) in
  xml_ok tag


let paste_rule_service = fun tag (cgi: Netcgi.cgi_activation) ->
  let update = update_of_string (cgi # argument_value "update") in
  let query = query_of_string (cgi # argument_value "wq") in
  let _ = Context.init_rule query (Context.Extr update) in
  xml_ok tag


let delete_rule_service = fun tag (cgi: Netcgi.cgi_activation) ->
  let query = query_of_string (cgi # argument_value "wq") in
  let _ = Context.init_rule query Context.Rem in
  xml_ok tag


let remove_rule_service = fun tag (cgi: Netcgi.cgi_activation) ->
  let rule_ids = List.map (fun x -> int_of_string x # value) (cgi # multiple_argument "ruleId") in
  let all_rules = Context.get_rules () in
  let remove_rule = fun id ->
    try
      Context.final_rule (List.nth all_rules id)
    with
    | Failure _ -> raise (Bad_rule_index id)
  in
  let _ = List.iter remove_rule rule_ids in
  xml_ok tag


(* -------------------------------------------------------------- *)
(* Reader commands *)

let load_from_lisfile = fun basename ->
    Context.load (!datadir // !context_name)

let load_from_ctxfile = fun () ->
  (* Loading from ctx: first reset, then import ctx, then save *)
  let _ = Context.init ~log:false !context_name in
  let options = [`Axioms; `Features; `Sources; `Extrinsic; `Wells; `Rules; `Actions] in
  let _ = awaken (fun () -> Context.import options $ !datadir // !context_name) in
  save_context ()

let autoload = fun () ->
  let lisfile = !datadir // (!context_name ^ ".lis") in
  let ctxfile = !datadir // (!context_name ^ ".ctx") in
  match Sys.file_exists lisfile, Sys.file_exists ctxfile with
  | true, true ->
    (* both files exists, check if ctx is more recent *)
    let lis_time = (Unix.stat lisfile).Unix.st_mtime in
    let ctx_time = (Unix.stat ctxfile).Unix.st_mtime in
    if (ctx_time -. lis_time > 5.) then
      let _ = Sys.remove lisfile in
      load_from_ctxfile ()
    else load_from_lisfile ()
  | true, false -> load_from_lisfile ()
  | false, true -> load_from_ctxfile ()
  | false, false ->
    (* Create an empty context *)
    let oc = open_out ctxfile in
    let _ = close_out oc in
    let options = [`Axioms; `Features; `Sources; `Extrinsic; `Wells; `Rules; `Actions] in
    let ctx = !datadir // !context_name in
    let _ = awaken (fun () -> Context.import options ctx) in
    save_context ()

let load_service = fun tag (cgi: Netcgi.cgi_activation) ->
  let _ = autoload () in
  xml_ok tag ~attrs:["contextName", !context_name]



let intent_service = fun tag (cgi: Netcgi.cgi_activation) ->
  let oids = List.map (fun x -> int_of_string x#value) (cgi # multiple_argument "oid") in
  let my_extent = Ext.union_r (List.map Ext.singleton oids) in
  (* compute intent of given oids *)
  let my_intent = Context.intent my_extent in
  let xml_intent = Xml.Element ("intent", [], List.map xml_of_feat my_intent) in
  let text_intent = xml_string $ String.concat " and " (List.map string_of_feat my_intent) in
  xml_ok tag ~attrs:["intentAsQuery", text_intent] ~children:[xml_intent]


(* Compute the normalized new working query after performing a zoom *)
let zoom_new_wq = fun old_wq feat ->
  let old_wq' = query_of_string old_wq in
  let incr = parse_of_string Context.parse_incr_query feat in
  let new_wq = incr old_wq' in
  string_of_query new_wq

let ls_features_service = fun tag (cgi : Netcgi.cgi_activation) ->
  let wq = url_decode $ cgi # argument_value "wq" in
  let feature = url_decode $ cgi # argument_value "feature" in
  let show_leaf_indicator = bool_of_string (cgi # argument_value "showLeaf" ~default:"false") in
  let new_wq = zoom_new_wq wq feature in
  let incrs, _locals, ext = ls wq feature in
  let nb_objects = Ext.cardinal ext in
  let nb_incrs = List.length incrs in
  let page_size = if (cgi # argument_exists "pageSize")
    then int_of_string (cgi # argument_value "pageSize")
    else nb_incrs in
  let page_number = if (cgi # argument_exists "page")
    then int_of_string (cgi # argument_value "page")
    else 0 in
  let start = page_number * page_size in
  let _ = if (start > nb_incrs) then raise Context_out_of_bounds in
  let incrs' = take page_size (drop start incrs) in
  let incrs'' = if show_leaf_indicator then
      let add_leaf_indic = fun (i, card) ->
        let sub_i, _, _ = ls new_wq i in
        let is_leaf = List.length sub_i = 0 in
        i, card, Some is_leaf
      in
      List.map add_leaf_indic incrs'
    else
      List.map (fun (i, c) -> i, c, None) incrs'
  in
  let xml_incrs = Xml.Element ("increments", [], List.map xml_of_incr' incrs'') in
  xml_ok tag
    ~attrs:["newWq", xml_string new_wq;
            "nbObjects", string_of_int nb_objects;
            "nbIncrs", string_of_int nb_incrs]
    ~children:[xml_incrs]

let get_tree_from_zoom_service = todo_service
let get_features_from_req_service = todo_service


let get_valued_features_for_oid = fun featnames oid ->
  let intent = List.map string_of_feat (Context.intent (Ext.singleton oid)) in
  let filters = List.map (fun s -> Str.regexp ("\\(" ^ s ^ "\\)\\( is \\| = \\| in \\)\\(.+\\)")) featnames in
  let rec split_feat = fun filters feat -> match filters with
    | [] -> None
    | x :: xs when Str.string_match x feat 0 ->
      Some (Str.matched_group 1 feat, Str.matched_group 3 feat)
    | x :: xs -> split_feat xs feat in
  let split_feats = List.map (split_feat filters) intent in
  let xml_of_featval = fun (name, value) ->
    let value' = if value.[0] = '"'
      then String.sub value 1 ((String.length value) - 2)
      else value in
    Xml.Element ("property", ["name", name; "value", value'], []) in
  let rec to_xml = fun l -> match l with
    | [] -> []
    | None :: xs -> to_xml xs
    | Some x :: xs -> (xml_of_featval x) :: (to_xml xs) in
  Xml.Element ("object", ["oid", (string_of_int oid)], (to_xml split_feats))


let get_valued_features_service = fun tag (cgi: Netcgi.cgi_activation) ->
  let _ = assert_params ["oid"] in         (* TODO: change to handle oid | req *)
  let oids = List.map (fun x -> int_of_string x#value) (cgi # multiple_argument "oid") in
  let featnames = List.map (fun x -> url_decode x#value) (cgi # multiple_argument "featname") in
  let featvalues = List.map (get_valued_features_for_oid featnames) oids in
  xml_ok tag ~children:featvalues



let extent_service = fun tag (cgi: Netcgi.cgi_activation) ->
  let wq = url_decode $ cgi # argument_value "wq" in
  let _incrs, _locals, full_ext = ls wq "all" in
  let card = Ext.cardinal full_ext in
  let page_size = if (cgi # argument_exists "pageSize")
    then int_of_string (cgi # argument_value "pageSize")
    else card in
  let page_number = if (cgi # argument_exists "page")
    then int_of_string (cgi # argument_value "page")
    else 0 in
  let start = page_number * page_size in
  let _ = if (start > card) then raise Context_out_of_bounds in
  let ext = take page_size (drop start (Ext.elements full_ext)) in
  let xml_ext = Xml.Element ("extent", [],
                             List.map ~f:xml_of_oid ext) in
  xml_ok tag ~attrs:["nbObjects", string_of_int card] ~children:[xml_ext]



let list_rules_service = fun tag (cgi: Netcgi.cgi_activation) ->
  let rules = Context.get_rules () in
  let xml_of_rule = fun i rule ->
    let query = string_of_query (Context.query_rule rule) in
    let xml_rule_attrs = match Context.update_rule rule with
      | Context.Intr u -> ["ruleId", (string_of_int i);
                           "kind", "paste intr";
                           "query", query;
                           "update", (string_of_update u)]
      | Context.Extr u -> ["ruleId", (string_of_int i);
                           "kind", "paste";
                           "query", query;
                           "update", (string_of_update u)]
      | Context.Rem -> ["ruleId", (string_of_int i);
                        "kind", "delete";
                        "query", query] in
    Xml.Element ("rule", xml_rule_attrs, []) in
  let xml_rules = mapi xml_of_rule rules in
  xml_ok tag ~children:xml_rules



(* -------------------------------------------------------------- *)
(* Dispatch services to URLs *)

(* list of services; each is a 5-uplet:
 *   URL,
 *   handler function,
 *   minimum role,
 *   list of required params,
 *   UpdateContext | UpdateUsers | NoChange
 *)
(* Note that presence of a "userKey" parameter is automatically checked
   if role <> No_right *)

type effect = UpdateContext | UpdateUsers | NoChange

let services = [ "/ping", ping_service, No_right, [], NoChange;
                 "/pingUsers", ping_users_service, Admin, [], NoChange;
                 "/setRole", set_role_service, No_right, ["adminKey"; "userKey"; "role"], UpdateUsers; (* TODO: change *key param names *)
                 "/registerKey", register_key_service, No_right, ["adminKey"; "userKey"], UpdateUsers; (* TODO: change *key param names *)
                 "/delKey", delkey_service, No_right, ["adminKey"; "userKey"], UpdateUsers; (* TODO: change *key param names *)
                 (* Admin commands *)
                 "/resetCamelis", reset_context_service, Admin, [], UpdateContext;
                 "/importCtx", import_context_service, Admin, ["ctxfile"], UpdateContext;
                 "/importLis", import_lis_service, Admin, ["lisfile"], UpdateContext;
                 "/importFile", import_file_service, Admin, ["file"], UpdateContext;
                 (* Collaborator commands *)
                 "/exportLis", export_lis_service, Collaborator, [], NoChange;
                 "/exportCtx", export_ctx_service, Collaborator, [], NoChange;
                 "/delObjects", del_objects_service, Collaborator, ["oid"], UpdateContext;
                 "/delFeatureFromReq", del_feature_from_req_service, Collaborator, ["wq"; "feature"; "reqForOids"], UpdateContext;
                 "/delFeatureFromOid", del_feature_from_oid_service, Collaborator, ["wq"; "feature"; "oid"], UpdateContext;
                 "/hideFeature", hide_feature_service, Collaborator, ["wq"; "feature"], UpdateContext;
                 "/createDeleteRule", delete_rule_service, Collaborator, ["wq"], UpdateContext;
                 "/removeRule", remove_rule_service, Collaborator, ["ruleId"], UpdateContext;
                 (* Publisher commands *)
                 "/setFeaturesFromReq", set_features_from_req_service, Publisher, ["wq"; "feature"; "reqForOids"], UpdateContext;
                 "/setFeaturesFromOids", set_features_from_oids_service, Publisher, ["wq"; "feature"; "oid"], UpdateContext;
                 "/createObject", create_object_service, Publisher, ["name"; "feature"], UpdateContext;
                 "/addFeatures", add_features_service, Publisher, [], UpdateContext;                   (* TODO *)
                 "/addAxiom", add_axiom_service, Publisher, ["premise"; "conclusion"], UpdateContext;
                 "/renameFeature", rename_feature_service, Publisher, ["feature"; "newName"], UpdateContext;
                 "/createPasteRule", paste_rule_service, Publisher, ["update"; "wq"], UpdateContext;
                 (* Reader commands *)
                 "/load", load_service, Reader, [], NoChange;
                 "/intent", intent_service, Reader, ["oid"], NoChange;
                 "/getIncrements", ls_features_service, Reader, ["wq"; "feature"], NoChange;
                 "/getTreeFromZoom", get_tree_from_zoom_service, Reader, [], NoChange; (* TODO *)
                 "/getFeaturesFromReq", get_features_from_req_service, Reader, [], NoChange; (* TODO *)
                 "/getValuedFeatures", get_valued_features_service, Reader, ["userKey"; "featname"], NoChange;
                 "/extent", extent_service, Reader, ["wq"], NoChange;
                 "/listRules", list_rules_service, Reader, [], NoChange;
               ]

(* Wrapper around service handlers *)
(* It checks for required params. It checks for access rights. It also
   catches exception fired by the handler, and send an xml
   representation of the error. *)
(* WARNING: the parameter for checking access rights is hardcoded to "userKey" *)
(* Also checks if the context has to be saved on disk *)
let wrap_handler = fun url handler role params update_kind env (cgi: Netcgi.cgi_activation) ->
  (* remove leading "/" from url to get tagname *)
  let tag = (Str.string_after url 1) ^ "Response" in
  try
    if role <> No_right then begin
      assert_params ("userKey"::params) cgi;
      (* assert_param "userKey" cgi; *)
      assert_rights (cgi # argument_value "userKey") role
    end
    else
      assert_params params cgi;
    let _ = logline !logfile (cgi # url ~with_query_string:`Env ()) in
    let _ = update_user_expiration (cgi # argument_value "userKey") in
    let xml = handler tag cgi in
    let _ = logline !logfile (url ^ " compute OK.") in
    let _ = if update_kind = UpdateContext then begin
      last_update := rfc3339_now ();
      save_context ();
    end in
    let _ = send_xml env cgi xml in
    logline !logfile (url ^ " all done.")
  with
  | e ->
    let xml = xml_error tag (xml_of_exn e) in
    send_xml env cgi xml


let send_resource = fun filename env cgi ->
 (* TODO: replace assert with 404 or error message *)
  let _ = assert (Sys.file_exists filename) in
  let mimetype = mimetype_of_filename filename in
  let data = string_of_file filename in
  env # set_output_header_field "Content-Type" mimetype;
  env # set_output_header_field "Content-Disposition" ("inline; filename=" ^ (Filename.basename filename));
  cgi # output # output_string data;
  cgi # output # commit_work ()


let get_resource_service = fun env (cgi: Netcgi.cgi_activation) ->
  let _ = assert_params ["userKey"; "file"] in
  let _ = assert_rights (cgi # argument_value "userKey") Reader in
  let filename = cgi # argument_value "file" in
  let real_filename = get_real_filename filename in
  send_resource real_filename env cgi


let default_thumb_width = "64"
let default_thumb_height = "64"

let mk_thumb_name = fun width height orig ->
  (* check if absolute or relative resource *)
  (Filename.dirname orig) // (Printf.sprintf "thumb-%s%,x%s-%s" width height (Filename.basename orig))


let get_thumb_service = fun env (cgi: Netcgi.cgi_activation) ->
  let _ = assert_params ["userKey"; "file"] in
  let _ = assert_rights (cgi # argument_value "userKey") Reader in
  let filename = cgi # argument_value "file" in
  let real_filename = get_real_filename filename in
  let width, height = match (cgi # argument_exists "width"), (cgi # argument_exists "height") with
    | true, true -> (cgi # argument_value "width"), cgi # argument_value "height"
    | false, false -> default_thumb_width, default_thumb_height
    | true, false -> (cgi # argument_value "width"), ""
    | false, true -> "", cgi # argument_value "height" in
  let thumbname = mk_thumb_name width height real_filename in
  (* TODO: check if file exists; if not, generate thumbnail *)
  if not (Sys.file_exists thumbname) then begin
    let cmd = Printf.sprintf "convert -resize %s%,x%s '%s' '%s'" width height real_filename thumbname in
    let _status = Sys.command cmd in
    ()
  end;
  send_resource thumbname env cgi


let upload_file_service = fun env (cgi: Netcgi.cgi_activation) ->
  let tag = "uploadResponse" in
  try
    let _ = assert_params ["userKey"; "file"] cgi in
    let _ = assert_rights (cgi # argument_value "userKey") Publisher in
    let file_arg = cgi # argument "file" in
    let server_file = match file_arg # store with
      | `File f -> f
      | `Memory -> failwith "The file was not correctly uploaded to the server" in
    let _ = logline !logfile server_file in
    let filename = match file_arg # filename with
      | Some name -> name
      | None -> failwith "No name was given for the uploaded file" in
    let dest_file = !datadir // filename in
    let force = bool_of_string (cgi # argument_value "force" ~default:"false") in
    let _ = if force then assert_rights (cgi # argument_value "userKey") Collaborator in
    let _ = if not force then assert_no_such_file dest_file in
    let _ = file_copy server_file dest_file in
    let _ = cgi # finalize () in
    let xml = xml_ok tag ~attrs:[] ~children:[] in
    send_xml env cgi xml
  with
  | e ->
    let xml = xml_error tag (xml_of_exn e) in
    send_xml env cgi xml


let srv =
  let make_service = fun (url, f, role, params, update_kind) ->
    url,
    dynamic_service { dyn_handler = wrap_handler url f role params update_kind;
	                  dyn_activation = std_activation `Std_activation_buffered;
	                  dyn_uri = None;
	                  dyn_translator = (fun _ -> "");
	                  dyn_accept_all_conditionals = false }
  in
  let lis_services = List.map make_service services in
  let resources_service =
    "/getResource",
    dynamic_service { dyn_handler = get_resource_service;
	                  dyn_activation = std_activation `Std_activation_buffered;
	                  dyn_uri = None;
	                  dyn_translator = (fun _ -> "");
	                  dyn_accept_all_conditionals = false }
  in
  let thumbs_service =
    "/getThumbnail",
    dynamic_service { dyn_handler = get_thumb_service;
	                  dyn_activation = std_activation `Std_activation_buffered;
	                  dyn_uri = None;
	                  dyn_translator = (fun _ -> "");
	                  dyn_accept_all_conditionals = false }
  in
  let upload_service =
    "/upload",
    dynamic_service { dyn_handler = upload_file_service;
	                  dyn_activation = std_activation `Std_activation_buffered;
	                  dyn_uri = None;
	                  dyn_translator = (fun _ -> "");
	                  dyn_accept_all_conditionals = false }
  in
  let all_services = resources_service :: thumbs_service :: upload_service :: lis_services in
  uri_distributor all_services



(* -------------------------------------------------------------- *)
(* Boilerplate code to create and launch web server *)

open Nethttpd_reactor

let start port =
  let config = Nethttpd_reactor.default_http_reactor_config in
  let master_sock = Unix.socket Unix.PF_INET Unix.SOCK_STREAM 0 in
  Unix.setsockopt master_sock Unix.SO_REUSEADDR true;
  Unix.bind master_sock (Unix.ADDR_INET(Unix.inet_addr_any, port));
  Unix.listen master_sock 100;
  Printf.printf "Listening on port %d\n" port;
  flush stdout;

  while true do
    try
      let conn_sock, _ = Unix.accept master_sock in
      Unix.set_nonblock conn_sock;
      let _ = Thread.create (process_connection config conn_sock) srv in
      ()
    with
      Unix.Unix_error(Unix.EINTR,_,_) -> ()  (* ignore *)
  done



(* -------------------------------------------------------------- *)
(* Parse command-line arguments and launches server *)

let usage = String.concat " "
  [ "Usage:"; Sys.argv.(0);
    "-port portnum";
    "-key adminKey";
    "-datadir string";
    "[-log filename]";
    "[-despot]";
    "\n"
  ]

let speclist =
  let add_creator = fun k -> add_user k Admin None in
  Arg.align
    [ ("-port",      Arg.Set_int port,           " port number the server will listen");
      ("-key",       Arg.String add_creator,     " key of the admin who owns the service");
      ("-datadir",   Arg.Set_string datadir,     " path to the directory holding context data on server");
      ("-log",       Arg.Set_string logfile,     " path to the log file (defaults to " ^ !logfile ^ ")");
      ("-despot",    Arg.Set ignore_rights,      " ignore users right (use for debugging purposes)");
    ]

let anon_fun = fun x -> raise $ Arg.Bad ("Bad argument : " ^ x)

(* TODO: check id is well formed? *)
let check_args = fun () ->
  let warn cond msg = if cond then Printf.printf "[Warning] %s\n" msg else () in
  let check cond msg = if cond then raise $ Arg.Bad msg else () in
  let in_argv str = array_mem str Sys.argv in
  let warnings = [ not (in_argv "-log"), "log not specified, defaulting to " ^ !logfile;
                 ] in
  let checks = [ not (in_argv "-port"), "Unspecified port number";
                 not (in_argv "-datadir"), "Unspecified datadir";
                 not (in_argv "-key"), "Unspecified key";
               ] in
  List.iter (uncurry check) checks;
  List.iter (uncurry warn) warnings

let main = fun () ->
  try
    Arg.parse speclist anon_fun usage;
    check_args ();
    (* derive context name from datadir, then load context *)
    context_name := Filename.basename !datadir;
    autoload();
    (* launch server *)
    Netsys_signal.init ();
    start !port
  with Arg.Bad msg ->
    print_endline $ "Error: " ^ msg;
    print_newline ();
    Arg.usage speclist usage

let _ = main ()
