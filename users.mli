type user_role = | No_right             (* cannot do anything *)
                 | Reader               (* can read context *)
                 | Publisher            (* can add but not delete *)
                 | Collaborator         (* can edit his context(s) *)
                 | Admin                (* can edit context and users *)
                 | General_admin        (* can do everything *)

type userkey = string                   (* hexadecimal number as a string *)
type date = float
type user = userkey * (user_role * date option)


exception No_such_role of string
exception Not_enough_rights
exception No_such_key of string
exception Expired_key of string
exception Registered_key of string

val default_key_timeout: float

val string_of_role: user_role -> string
val role_of_string: string -> user_role

val user_list: unit -> user list
val get_user: userkey -> user_role * date option
val update_user_role: userkey -> user_role -> unit
val update_user_expiration: userkey -> unit
val add_user: userkey -> user_role -> date option -> unit
val remove_user: userkey -> unit

val assert_rights: userkey -> user_role -> unit
val assert_new_key: userkey -> unit
val xml_of_user: user -> Xml.xml
val xml_of_key: userkey -> Xml.xml
