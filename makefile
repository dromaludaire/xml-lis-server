NAME = xml-lis-server

OCAMLC   = ocamlfind ocamlc
OCAMLOPT = ocamlfind ocamlopt


INCLUDES = -package threads,str,nethttpd,xml-light,camelis
PP =
FLAGS = -thread $(PP) $(INCLUDES)    # add other options for ocamlc here

SRC = glogic.ml utils.ml mimetypes.ml users.ml main.ml

OBJECTS  = $(SRC:%.ml=%.cmo)
XOBJECTS = $(OBJECTS:%.cmo=%.cmx)

EXEC = $(NAME).exe

all: $(EXEC)

$(EXEC): $(XOBJECTS)
	$(OCAMLOPT) $(FLAGS) -o $@ -linkpkg $^

# ---------------------------------------------------------------------
# Optional bytecode compilation
#
byte: $(OBJECTS)
	$(OCAMLC) $(FLAGS) -o $(NAME).byte.exe -linkpkg $^


# ----------------------------------------------------------------------
# Common rules

%.cmi: %.mli
	$(OCAMLC) $(FLAGS) -c $<

%.cmo: %.ml
	$(OCAMLC) $(FLAGS) -c $<

%.cmx: %.ml
	$(OCAMLOPT) $(FLAGS) -c $<

%.cmxs: %.cmxa
	$(OCAMLOPT) -shared -linkall -o $@ $<

users.cmo: users.cmi
users.cmx: users.cmi

# ----------------------------------------------------------------------
# Clean up

.PHONY: clean

clean:
	rm -f *.cm[ioax]
	rm -f *.o
	rm -f *.a
	rm -f *.cmx[as]
	rm -f *.exe
	rm -f *.zip


# ----------------------------------------------------------------------
#  Create a tarball of the sources

.PHONY: dist package

zipdist:
	zip $(NAME) $(SRC) users.mli makefile test.html

package: all zipdist


# ----------------------------------------------------------------------
#  Run server

.PHONY: run-test

TESTDIR = /tmp/planets

runtest: $(EXEC)
	rm -rf $(TESTDIR)
	mkdir -p $(TESTDIR)/images
	install planets.ctx $(TESTDIR)
	install mpthreetest.mp3 $(TESTDIR)
	install images/* $(TESTDIR)/images
	xdg-open test.html
	./xml-lis-server.exe -port 9999 -key a243F -datadir /tmp/planets
